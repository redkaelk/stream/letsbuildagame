﻿using System;
using System.Runtime.InteropServices;

namespace LetsBuildAGame
{
    public static class MarshalStructSerializer
    {
        public static T Deserialize<T>(byte[] bytes) where T : struct
        {
            if (bytes is null)
            {
                throw new ArgumentNullException(nameof(bytes));
            }

            var size = Marshal.SizeOf<T>();
            if (bytes.Length != size)
            {
                throw new ArgumentOutOfRangeException(nameof(bytes), "Length of the array is not equal to the size of the struct.");
            }

            var ptr = IntPtr.Zero;
            try
            {
                ptr = Marshal.AllocHGlobal(size);
                Marshal.Copy(bytes, 0, ptr, size);
                var structure = Marshal.PtrToStructure<T>(ptr);
                return structure;
            }
            finally
            {
                if (ptr != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(ptr);
                }
            }
        }

        public static byte[] Serialize<T>(T structure) where T : struct
        {
            var size = Marshal.SizeOf<T>();

            var ptr = IntPtr.Zero;
            try
            {
                ptr = Marshal.AllocHGlobal(size);
                Marshal.StructureToPtr(structure, ptr, fDeleteOld: false);
                var bytes = new byte[size];
                Marshal.Copy(ptr, bytes, 0, size);
                return bytes;
            }
            finally
            {
                if (ptr != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(ptr);
                }
            }
        }
    }
}
