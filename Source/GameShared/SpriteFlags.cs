using System;

namespace LetsBuildAGame
{
    [Flags]
    public enum SpriteFlags
    {
        None = 0,
        NetworkPositionChanged = 1,
        NetworkRotationChanged = 2,
        AllPositionChanged = NetworkPositionChanged,
        AllRotationChanged = NetworkRotationChanged,
    }
}