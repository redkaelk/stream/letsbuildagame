﻿using Lidgren.Network;
using System;

namespace LetsBuildAGame.Networking
{
    public abstract class BaseMessageHandler<TMessage, TGame> : IMessageHandler where TMessage : struct
    {
        public TGame Game
        {
            get;
        }

        public BaseMessageHandler(TGame game)
        {
            if (game is null)
            {
                throw new ArgumentNullException(nameof(game));
            }

            this.Game = game;
        }

        public void Handle(NetIncomingMessage message)
        {
            var structure = message.ReadStructure<TMessage>();
            this.Handle(structure, message.SenderConnection);
        }

        protected abstract void Handle(TMessage message, NetConnection senderConnection);

        public virtual void Initialize()
        {
            // do nothing
        }
    }
}
