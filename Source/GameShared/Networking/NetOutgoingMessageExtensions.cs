﻿using Lidgren.Network;
using Microsoft.Xna.Framework;

namespace LetsBuildAGame.Networking
{
    public static class NetOutgoingMessageExtensions
    {
        public static void Write<T>(this NetOutgoingMessage message, T structure) where T : struct
        {
            var bytes = MarshalStructSerializer.Serialize(structure);
            message.Write(bytes);
        }

        public static void Write(this NetOutgoingMessage message, Vector2 vector)
        {
            message.Write(vector.X);
            message.Write(vector.Y);
        }
    }
}
