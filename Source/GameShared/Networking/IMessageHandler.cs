﻿using Lidgren.Network;

namespace LetsBuildAGame.Networking
{
    public interface IMessageHandler
    {
        void Handle(NetIncomingMessage message);
        void Initialize();
    }
}
