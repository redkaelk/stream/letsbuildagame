﻿namespace LetsBuildAGame.Networking
{
    public enum ServerToClientMessageId : byte
    {
        JoinResponse = 0,
        CreateSprite = 1,
        UpdateSpritePositionRotation = 2,
    }
}
