﻿namespace LetsBuildAGame.Networking
{
    public enum ClientToServerMessageId : byte
    {
        JoinRequest = 0,
        PlayerInput = 1,
    }
}
