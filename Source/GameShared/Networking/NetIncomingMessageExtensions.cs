﻿using Lidgren.Network;
using Microsoft.Xna.Framework;
using System.Runtime.InteropServices;

namespace LetsBuildAGame.Networking
{
    public static class NetIncomingMessageExtensions
    {
        public static T ReadStructure<T>(this NetIncomingMessage message) where T : struct
        {
            var size = Marshal.SizeOf<T>();
            var bytes = message.ReadBytes(size);
            var structure = MarshalStructSerializer.Deserialize<T>(bytes);
            return structure;
        }

        public static Vector2 ReadVector2(this NetIncomingMessage message)
        {
            var x = message.ReadSingle();
            var y = message.ReadSingle();
            var vector = new Vector2(x, y);
            return vector;
        }
    }
}
