﻿using Microsoft.Xna.Framework;

namespace LetsBuildAGame.Networking.Messages
{
    public struct UpdateSpritePositionRotationMessage
    {
        public ServerToClientMessageId MessageId;
        public long Id;
        public Vector2 Position;
        public float Rotation;
    }
}
