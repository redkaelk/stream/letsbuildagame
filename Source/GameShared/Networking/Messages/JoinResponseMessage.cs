﻿namespace LetsBuildAGame.Networking.Messages
{
    public struct JoinResponseMessage
    {
        public ServerToClientMessageId MessageId;
        public bool WasJoined;
        public long PlayerSpriteId;
    }
}
