﻿using System.Runtime.InteropServices;

namespace LetsBuildAGame.Networking.Messages
{
    public struct JoinRequestMessage
    {
        public ClientToServerMessageId MessageId;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 40)]
        public string DisplayName;
    }
}
