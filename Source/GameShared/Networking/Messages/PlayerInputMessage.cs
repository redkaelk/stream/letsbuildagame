﻿namespace LetsBuildAGame.Networking.Messages
{
    public struct PlayerInputMessage
    {
        public ClientToServerMessageId MessageId;
        public sbyte HorizontalDirection;
        public sbyte VerticalDirection;
    }
}
