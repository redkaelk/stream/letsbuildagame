﻿using Microsoft.Xna.Framework;
using System.Runtime.InteropServices;

namespace LetsBuildAGame.Networking.Messages
{
    public struct CreateSpriteMessage
    {
        public ServerToClientMessageId MessageId;
        public long Id;
        public Vector2 Origin;
        public Vector2 Position;
        public float Rotation;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string TextureName;
        public Vector2 Scale;
        public float LayerDepth;
    }
}
