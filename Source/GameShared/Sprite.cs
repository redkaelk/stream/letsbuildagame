﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LetsBuildAGame
{
    public class Sprite
    {
        public Color Color
        {
            get;
            set;
        } = Color.White;

        public long Id
        {
            get;
            set;
        }

        public float LayerDepth
        {
            get;
            set;
        } = 0.5f;

        public Vector2 Origin
        {
            get;
            set;
        }

        public Vector2 Position
        {
            get;
            set;
        }

        public float Rotation
        {
            get;
            set;
        }

        public Vector2 Scale
        {
            get;
            set;
        } = Vector2.One;

        public SpriteEffects SpriteEffects
        {
            get;
            set;
        }

        public Texture2D Texture
        {
            get;
            set;
        }

        public string TextureName
        {
            get;
            set;
        }

        public SpriteFlags Flags
        {
            get;
            set;
        }
    }
}
