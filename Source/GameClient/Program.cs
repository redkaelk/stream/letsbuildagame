﻿using LetsBuildAGame.Cameras;
using LetsBuildAGame.ClearGraphicsDevice;
using LetsBuildAGame.Screenery;
using LetsBuildAGame.Sprites;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using System;

namespace LetsBuildAGame
{
    public static class Program
    {
        [STAThread]
        private static void Main()
        {
            using (var game = new Game())
            {
                _ = new GraphicsDeviceManager(game);
                game.Content.RootDirectory = "Content";
                game.IsMouseVisible = true;
                game.Window.AllowUserResizing = true;

                var screenComponent = new ScreenComponent(game);
                var screenService = (IScreenService)screenComponent;
                screenService.SwitchTo(Screens.MadeWithMonoGameSplash);

                var networkConfiguration = new NetPeerConfiguration(NetworkConstants.AppIdentifier);
                var networkClient = new NetClient(networkConfiguration);

                game.Services.AddService(new SpriteCollections());
                game.Services.AddService<ICameraService>(new CameraService());
                game.Services.AddService(screenService);
                game.Services.AddService(networkClient);

                var components = game.Components;
                components.Add(new ClearGraphicsDeviceComponent(game));
                components.Add(screenComponent);

                game.Run();
            }
        }
    }
}
