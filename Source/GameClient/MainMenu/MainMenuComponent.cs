﻿using LetsBuildAGame.Screenery;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace LetsBuildAGame.MainMenu
{
    public class MainMenuComponent : DrawableGameComponent
    {
        private Texture2D texture;
        private Vector2 origin;
        private SpriteBatch spriteBatch;
        private Button[] buttons;
        private KeyboardState previousKeyboard;

        public MainMenuComponent(Game game) : base(game)
        {
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            var width = this.GraphicsDevice.PresentationParameters.BackBufferWidth;
            var height = this.GraphicsDevice.PresentationParameters.BackBufferHeight;
            var position = new Vector2(width * 0.5f, height * 0.5f);
            var horizontalScale = width / (float)this.texture.Width;
            var verticalScale = height / (float)this.texture.Height;
            var scale = MathF.Min(horizontalScale, verticalScale);

            var spriteBatch = this.spriteBatch;
            spriteBatch.Begin();
            spriteBatch.Draw(this.texture, position, null, Color.White, 0.0f, this.origin, scale, SpriteEffects.None, 1.0f);
            spriteBatch.End();
        }

        public override void Initialize()
        {
            base.Initialize();
            this.texture = this.Game.Content.Load<Texture2D>("MainMenu/MainMenu");
            this.origin = this.texture.Bounds.Size.ToVector2() * 0.5f;
            this.spriteBatch = new SpriteBatch(this.GraphicsDevice);
            this.InitializeButtons();
        }

        private void InitializeButtons()
        {
            this.buttons = new Button[]
            {
                new Button()
                {
                    Callback = this.Play,
                    Key = Keys.P,
                },
                new Button()
                {
                    Callback = this.Quit,
                    Key = Keys.Q,
                },
            };
        }

        private void Play()
        {
            var screenService = this.Game.Services.GetService<IScreenService>();
            screenService.SwitchTo(Screens.Connecting);
        }

        private void Quit()
        {
            this.Game.Exit();
            Environment.Exit(0);
        }

        protected override void UnloadContent()
        {
            this.spriteBatch.Dispose();
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            var keyboard = Keyboard.GetState();

            foreach (var button in this.buttons)
            {
                if (this.previousKeyboard.IsKeyDown(button.Key) && keyboard.IsKeyUp(button.Key))
                {
                    button.Callback();
                }
            }

            this.previousKeyboard = keyboard;
        }
    }
}
