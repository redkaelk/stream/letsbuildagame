﻿using Microsoft.Xna.Framework.Input;
using System;

namespace LetsBuildAGame.MainMenu
{
    public class Button
    {
        public Action Callback
        {
            get;
            set;
        }

        public Keys Key
        {
            get;
            set;
        }
    }
}
