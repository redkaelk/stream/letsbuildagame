﻿using LetsBuildAGame.Screenery;
using Microsoft.Xna.Framework;
using System;

namespace LetsBuildAGame.MainMenu
{
    public class MainMenuScreen : Screen
    {
        public MainMenuScreen()
        {
            this.BackgroundColor = new Color(255, 106, 0);
            this.ComponentTypes = new Type[]
            {
                typeof(MainMenuComponent),
            };
        }
    }
}
