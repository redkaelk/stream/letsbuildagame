﻿using LetsBuildAGame.Connecting;
using LetsBuildAGame.InGame;
using LetsBuildAGame.MainMenu;
using LetsBuildAGame.Splash;

namespace LetsBuildAGame.Screenery
{
    public static class Screens
    {
        public static Screen Connecting
        {
            get;
            set;
        } = new ConnectingScreen();

        public static Screen InGame
        {
            get;
            set;
        } = new InGameScreen();

        public static Screen MadeWithMonoGameSplash
        {
            get;
            set;
        } = new MadeWithMonoGameSplashScreen();

        public static Screen MainMenu
        {
            get;
            set;
        } = new MainMenuScreen();

        public static Screen LetsBuildAGameSplash
        {
            get;
            set;
        } = new LetsBuildAGameSplashScreen();
    }
}
