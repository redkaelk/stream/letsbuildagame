﻿using LetsBuildAGame.ClearGraphicsDevice;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace LetsBuildAGame.Screenery
{
    public class ScreenComponent : GameComponent, IScreenService
    {
        private Screen nextScreen;
        private Screen currentScreen;
        private ClearGraphicsDeviceOptions clearGraphicsDeviceOptions;
        private readonly List<IGameComponent> currentComponents = new List<IGameComponent>();

        public ScreenComponent(Game game) : base(game)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
            this.clearGraphicsDeviceOptions = this.Game.Services.GetService<ClearGraphicsDeviceOptions>();
        }

        private void LoadNextScreen()
        {
            var screen = this.nextScreen;

            foreach (var componentType in screen.ComponentTypes)
            {
                var component = (IGameComponent)Activator.CreateInstance(componentType, this.Game);
                this.currentComponents.Add(component);
                this.Game.Components.Add(component);
            }

            this.clearGraphicsDeviceOptions.Color = screen.BackgroundColor;

            this.currentScreen = screen;
            this.nextScreen = null;
        }

        private void Switch()
        {
            if (this.nextScreen == null)
            {
                return;
            }

            this.UnloadCurrentScreen();
            this.LoadNextScreen();
        }

        void IScreenService.SwitchTo(Screen screen)
        {
            this.nextScreen = screen;
        }

        private void UnloadCurrentScreen()
        {
            if (this.currentScreen == null)
            {
                return;
            }

            foreach (var component in this.currentComponents)
            {
                _ = this.Game.Components.Remove(component);
            }
            this.currentComponents.Clear();

            this.currentScreen = null;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            this.Switch();
        }
    }
}
