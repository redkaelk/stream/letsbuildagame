﻿namespace LetsBuildAGame.Screenery
{
    public interface IScreenService
    {
        void SwitchTo(Screen screen);
    }
}
