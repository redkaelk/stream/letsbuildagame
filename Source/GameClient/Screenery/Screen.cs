﻿using Microsoft.Xna.Framework;
using System;

namespace LetsBuildAGame.Screenery
{
    public class Screen
    {
        public Color BackgroundColor
        {
            get;
            set;
        }

        public Type[] ComponentTypes
        {
            get;
            set;
        }
    }
}
