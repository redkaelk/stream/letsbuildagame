﻿using Microsoft.Xna.Framework;

namespace LetsBuildAGame.ClearGraphicsDevice
{
    public class ClearGraphicsDeviceComponent : DrawableGameComponent
    {
        private ClearGraphicsDeviceOptions options;

        public ClearGraphicsDeviceComponent(Game game) : base(game)
        {
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            this.GraphicsDevice.Clear(this.options.Color);
        }

        public override void Initialize()
        {
            base.Initialize();
            this.options = new ClearGraphicsDeviceOptions();
            this.Game.Services.AddService(this.options);
        }
    }
}
