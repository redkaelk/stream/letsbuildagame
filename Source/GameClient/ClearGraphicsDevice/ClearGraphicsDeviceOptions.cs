﻿using Microsoft.Xna.Framework;

namespace LetsBuildAGame.ClearGraphicsDevice
{
    public class ClearGraphicsDeviceOptions
    {
        public Color Color
        {
            get;
            set;
        } = Color.CornflowerBlue;
    }
}
