﻿using LetsBuildAGame.Networking;
using LetsBuildAGame.Networking.Messages;
using LetsBuildAGame.Sprites;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace LetsBuildAGame.InGame.MessageHandlers
{
    public class UpdateSpritePositionRotationMessageHandler : BaseMessageHandler<UpdateSpritePositionRotationMessage, Game>
    {
        private SpriteCollections sprites;

        public UpdateSpritePositionRotationMessageHandler(Game game) : base(game)
        {
        }

        protected override void Handle(UpdateSpritePositionRotationMessage message, NetConnection senderConnection)
        {
            var sprite = this.sprites.ById.GetValueOrDefault(message.Id);
            if (sprite == null)
            {
                return;
            }

            sprite.Position = message.Position;
            sprite.Rotation = message.Rotation;
        }

        public override void Initialize()
        {
            base.Initialize();

            var services = this.Game.Services;
            this.sprites = services.GetService<SpriteCollections>();
        }
    }
}
