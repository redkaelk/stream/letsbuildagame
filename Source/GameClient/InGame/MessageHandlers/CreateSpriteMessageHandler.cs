﻿using LetsBuildAGame.Networking;
using LetsBuildAGame.Networking.Messages;
using LetsBuildAGame.Sprites;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace LetsBuildAGame.InGame.MessageHandlers
{
    public class CreateSpriteMessageHandler : BaseMessageHandler<CreateSpriteMessage, Game>
    {
        private SpriteCollections sprites;

        public CreateSpriteMessageHandler(Game game) : base(game)
        {
        }

        protected override void Handle(CreateSpriteMessage message, NetConnection senderConnection)
        {
            var sprite = this.sprites.ById.GetValueOrDefault(message.Id);
            if (sprite != null)
            {
                return;
            }

            var textureName = message.TextureName;
            if (string.IsNullOrWhiteSpace(textureName))
            {
                throw new Exception("TextureName not specified");
            }

            var texture = this.Game.Content.Load<Texture2D>(textureName);
            var origin = texture.Bounds.Size.ToVector2() * message.Origin;

            sprite = new Sprite()
            {
                Id = message.Id,
                Scale = message.Scale,
                LayerDepth = message.LayerDepth,
                Position = message.Position,
                Rotation = message.Rotation,
                Texture = texture,
                Origin = origin,
            };
            _ = this.sprites.All.Add(sprite);
            this.sprites.ById.Add(message.Id, sprite);
        }

        public override void Initialize()
        {
            base.Initialize();

            var services = this.Game.Services;
            this.sprites = services.GetService<SpriteCollections>();
        }
    }
}
