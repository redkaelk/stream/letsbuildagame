﻿using LetsBuildAGame.Cameras;
using LetsBuildAGame.Screenery;
using LetsBuildAGame.Sprites;
using Microsoft.Xna.Framework;
using System;

namespace LetsBuildAGame.InGame
{
    public class InGameScreen : Screen
    {
        public InGameScreen()
        {
            this.BackgroundColor = Color.CornflowerBlue;
            this.ComponentTypes = new Type[]
            {
                typeof(ReceiverNetworkComponent),
                typeof(CameraComponent),
                typeof(SpriteComponent),
                typeof(PlayerInputComponent),
            };
        }
    }
}
