﻿using LetsBuildAGame.Networking;
using LetsBuildAGame.Networking.Messages;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace LetsBuildAGame.InGame
{
    public class PlayerInputComponent : GameComponent
    {
        private NetClient client;

        public PlayerInputComponent(Game game) : base(game)
        {
        }

        public override void Initialize()
        {
            base.Initialize();

            var services = this.Game.Services;
            this.client = services.GetService<NetClient>();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            var horizontalDirection = (sbyte)0;
            var verticalDirection = (sbyte)0;
            var keyboard = Keyboard.GetState();
            if (keyboard.IsKeyDown(Keys.A))
            {
                // move character left
                horizontalDirection = -1;
            }
            else if (keyboard.IsKeyDown(Keys.D))
            {
                // move character right
                horizontalDirection = 1;
            }
            if (keyboard.IsKeyDown(Keys.W))
            {
                // move character up
                verticalDirection = -1;
            }
            else if (keyboard.IsKeyDown(Keys.S))
            {
                // move character down
                verticalDirection = 1;
            }

            var playerInput = new PlayerInputMessage()
            {
                MessageId = ClientToServerMessageId.PlayerInput,
                HorizontalDirection = horizontalDirection,
                VerticalDirection = verticalDirection,
            };

            var message = this.client.CreateMessage();
            message.Write(playerInput);
            _ = this.client.SendMessage(message, NetDeliveryMethod.UnreliableSequenced, 1);
        }
    }
}
