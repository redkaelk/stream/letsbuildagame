﻿using LetsBuildAGame.InGame.MessageHandlers;
using LetsBuildAGame.Networking;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace LetsBuildAGame.InGame
{
    public class ReceiverNetworkComponent : GameComponent
    {
        private readonly IMessageHandler[] handlers = new IMessageHandler[256];
        private NetClient client;

        public ReceiverNetworkComponent(Game game) : base(game)
        {
            this.SetHandler<CreateSpriteMessageHandler>(ServerToClientMessageId.CreateSprite);
            this.SetHandler<UpdateSpritePositionRotationMessageHandler>(ServerToClientMessageId.UpdateSpritePositionRotation);
        }

        private void HandleData(NetIncomingMessage message)
        {
            var index = message.PeekByte();
            var handler = this.handlers[index];
            if (handler == null)
            {
                return;
            }

            handler.Handle(message);
        }

        public override void Initialize()
        {
            base.Initialize();
            this.InitializeExistingHandlers();

            var services = this.Game.Services;
            this.client = services.GetService<NetClient>();
        }

        private void InitializeExistingHandlers()
        {
            foreach (var handler in this.handlers)
            {
                if (handler == null)
                {
                    continue;
                }

                handler.Initialize();
            }
        }

        private void SetHandler<THandler>(ServerToClientMessageId id) where THandler : IMessageHandler
        {
            var index = (byte)id;
            var handler = (IMessageHandler)Activator.CreateInstance(typeof(THandler), this.Game);
            this.handlers[index] = handler;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            var messages = new List<NetIncomingMessage>();
            _ = this.client.ReadMessages(messages);
            foreach (var message in messages)
            {
                switch (message.MessageType)
                {
                    case NetIncomingMessageType.Data:
                        this.HandleData(message);
                        break;
                }

                this.client.Recycle(message);
            }
        }
    }
}
