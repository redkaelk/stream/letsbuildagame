﻿using LetsBuildAGame.Networking;
using LetsBuildAGame.Networking.Messages;
using LetsBuildAGame.Screenery;
using LetsBuildAGame.Sprites;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LetsBuildAGame.Connecting
{
    public class ConnectingNetworkComponent : GameComponent
    {
        private NetClient client;
        private Action state;
        private SpriteCollections sprites;

        public ConnectingNetworkComponent(Game game) : base(game)
        {
            this.state = this.ConnectToServerState;
        }

        private void ConnectToServerState()
        {
            _ = this.client.Connect("127.0.0.1", NetworkConstants.DefaultPort);
            this.state = this.WaitForConnectionState;
        }

        private void DoneState()
        {
            // do nothing
        }

        public override void Initialize()
        {
            base.Initialize();

            var services = this.Game.Services;
            this.sprites = services.GetService<SpriteCollections>();
            this.client = services.GetService<NetClient>();
            this.client.Start();
        }

        private void JoinGameState()
        {
            if (this.client.ServerConnection == null)
            {
                return;
            }

            var joinRequest = new JoinRequestMessage()
            {
                MessageId = ClientToServerMessageId.JoinRequest,
                DisplayName = "bob",
            };

            var message = this.client.CreateMessage();
            message.Write(joinRequest);
            _ = this.client.SendMessage(message, NetDeliveryMethod.ReliableOrdered, 0);

            this.state = this.WaitForReplyState;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            this.state();
        }

        private void WaitForConnectionState()
        {
            var messages = new List<NetIncomingMessage>();
            _ = this.client.ReadMessages(messages);
            foreach (var message in messages)
            {
                if (message.MessageType != NetIncomingMessageType.StatusChanged)
                {
                    continue;
                }

                var status = (NetConnectionStatus)message.ReadByte();
                var statusMessage = message.ReadString();
                Debug.WriteLine($"Status changed on client {status} {statusMessage}");

                if (status == NetConnectionStatus.Connected)
                {
                    this.state = this.JoinGameState;
                }
                else if (status == NetConnectionStatus.Disconnected)
                {
                    this.state = this.ConnectToServerState;
                }
            }
        }

        private void WaitForReplyState()
        {
            var message = this.client.ReadMessage();
            if (message == null)
            {
                return;
            }
            if (message.MessageType != NetIncomingMessageType.Data)
            {
                return;
            }

            var id = message.PeekByte();
            if (id != 0)
            {
                return;
            }

            var joinResponse = message.ReadStructure<JoinResponseMessage>();

            var screenService = this.Game.Services.GetService<IScreenService>();
            var wasJoined = joinResponse.WasJoined;
            if (wasJoined)
            {
                this.sprites.PlayerSpriteId = joinResponse.PlayerSpriteId;
                screenService.SwitchTo(Screens.InGame);
            }
            else
            {
                screenService.SwitchTo(Screens.MainMenu);
            }

            this.state = this.DoneState;
        }
    }
}
