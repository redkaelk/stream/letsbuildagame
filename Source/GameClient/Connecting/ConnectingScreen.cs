﻿using LetsBuildAGame.Screenery;
using Microsoft.Xna.Framework;
using System;

namespace LetsBuildAGame.Connecting
{
    public class ConnectingScreen : Screen
    {
        public ConnectingScreen()
        {
            this.BackgroundColor = Color.GreenYellow;
            this.ComponentTypes = new Type[]
            {
                typeof(ConnectingNetworkComponent),
            };
        }
    }
}
