﻿using LetsBuildAGame.Cameras;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LetsBuildAGame.Sprites
{
    public class SpriteComponent : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private SpriteCollections sprites;
        private ICameraService cameraService;

        public SpriteComponent(Game game) : base(game)
        {
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            var world = this.cameraService.Current.World;
            var spriteBatch = this.spriteBatch;
            spriteBatch.Begin(transformMatrix: world, sortMode: SpriteSortMode.FrontToBack);
            foreach (var sprite in this.sprites.All)
            {
                if (sprite.Texture == null)
                {
                    continue;
                }

                spriteBatch.Draw(sprite.Texture, sprite.Position, null, sprite.Color, sprite.Rotation, sprite.Origin, sprite.Scale, sprite.SpriteEffects, sprite.LayerDepth);
            }
            spriteBatch.End();
        }

        public override void Initialize()
        {
            base.Initialize();

            var services = this.Game.Services;
            this.sprites = services.GetService<SpriteCollections>();
            this.cameraService = services.GetService<ICameraService>();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            this.spriteBatch = new SpriteBatch(this.GraphicsDevice);
        }
    }
}
