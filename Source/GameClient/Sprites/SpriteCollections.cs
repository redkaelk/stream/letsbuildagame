﻿using System.Collections.Generic;

namespace LetsBuildAGame.Sprites
{
    public class SpriteCollections
    {
        public HashSet<Sprite> All
        {
            get;
        } = new HashSet<Sprite>();

        public Dictionary<long, Sprite> ById
        {
            get;
        } = new Dictionary<long, Sprite>();

        public long PlayerSpriteId
        {
            get;
            set;
        }
    }
}
