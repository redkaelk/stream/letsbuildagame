using LetsBuildAGame.Sprites;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace LetsBuildAGame.Cameras
{
    public class CameraComponent : GameComponent
    {
        private ICameraService cameraService;
        private SpriteCollections sprites;

        public CameraComponent(Game game) : base(game)
        {
        }

        public override void Initialize()
        {
            base.Initialize();

            var services = this.Game.Services;
            this.cameraService = services.GetService<ICameraService>();
            this.sprites = services.GetService<SpriteCollections>();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            this.UpdateTarget();
            this.UpdateCurrent(gameTime);
            this.UpdateMatrix();
        }

        private void UpdateTarget()
        {
            var playerSprite = this.sprites.ById.GetValueOrDefault(this.sprites.PlayerSpriteId);
            if(playerSprite == null)
            {
                return;
            }

            this.cameraService.Current.TargetPosition = playerSprite.Position;
        }

        private void UpdateCurrent(GameTime gameTime)
        {
            var camera = this.cameraService.Current;
            var speed = (float)(gameTime.ElapsedGameTime.TotalSeconds * camera.PositionSpeed);
            camera.CurrentPosition = Vector2.Lerp(camera.CurrentPosition, camera.TargetPosition, speed);
        }

        private void UpdateMatrix()
        {
            var cameraPosition = this.cameraService.Current.CurrentPosition;
            var clientBounds = this.Game.Window.ClientBounds;
            this.cameraService.Current.World
                = Matrix.Identity
                * Matrix.CreateTranslation(-cameraPosition.X, -cameraPosition.Y, 0.0f)
                * Matrix.CreateTranslation(clientBounds.Width * 0.5f, clientBounds.Height * 0.5f, 0.0f)
                ;
        }
    }
}