using Microsoft.Xna.Framework;

namespace LetsBuildAGame.Cameras
{
    public class Camera
    {
        public Vector2 TargetPosition
        {
            get;
            set;
        }

        public Vector2 CurrentPosition
        {
            get;
            set;
        }

        public float PositionSpeed
        {
            get;
            set;
        } = 0.3f;

        public Matrix World
        {
            get;
            set;
        }
    }
}