namespace LetsBuildAGame.Cameras
{
    public class CameraService : ICameraService
    {
        public Camera Current
        {
            get;
            set;
        } = new Camera();
    }
}