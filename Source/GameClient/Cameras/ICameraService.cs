namespace LetsBuildAGame.Cameras
{
    public interface ICameraService
    {
        Camera Current
        {
            get;
            set;
        }
    }
}