﻿using LetsBuildAGame.Screenery;
using Microsoft.Xna.Framework;

namespace LetsBuildAGame.Splash
{
    public class MadeWithMonoGameSplashComponent : BaseSplashComponent
    {
        public MadeWithMonoGameSplashComponent(Game game) : base(game, "Splash/MadeWithMonoGame", Screens.LetsBuildAGameSplash)
        {
        }
    }
}
