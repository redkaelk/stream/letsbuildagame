﻿using LetsBuildAGame.Screenery;
using Microsoft.Xna.Framework;
using System;

namespace LetsBuildAGame.Splash
{
    public class MadeWithMonoGameSplashScreen : Screen
    {
        public MadeWithMonoGameSplashScreen()
        {
            this.BackgroundColor = Color.White;
            this.ComponentTypes = new Type[]
            {
                typeof(MadeWithMonoGameSplashComponent),
            };
        }
    }
}
