﻿using LetsBuildAGame.Screenery;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace LetsBuildAGame.Splash
{
    public abstract class BaseSplashComponent : DrawableGameComponent
    {
        private readonly string textureName;
        private readonly Screen nextScreen;
        private Texture2D texture;
        private Vector2 origin;
        private SpriteBatch spriteBatch;
        private double timer;
        private KeyboardState previousKeyboard;
        private bool isSwitching;

        public BaseSplashComponent(Game game, string textureName, Screen nextScreen) : base(game)
        {
            if (string.IsNullOrWhiteSpace(textureName))
            {
                throw new ArgumentException($"'{nameof(textureName)}' cannot be null or whitespace.", nameof(textureName));
            }
            if (nextScreen is null)
            {
                throw new ArgumentNullException(nameof(nextScreen));
            }

            this.textureName = textureName;
            this.nextScreen = nextScreen;
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);

            var width = this.GraphicsDevice.PresentationParameters.BackBufferWidth;
            var height = this.GraphicsDevice.PresentationParameters.BackBufferHeight;
            var position = new Vector2(width * 0.5f, height * 0.5f);
            var horizontalScale = width / (float)this.texture.Width;
            var verticalScale = height / (float)this.texture.Height;
            var scale = MathF.Min(horizontalScale, verticalScale);

            var spriteBatch = this.spriteBatch;
            spriteBatch.Begin();
            spriteBatch.Draw(this.texture, position, null, Color.White, 0.0f, this.origin, scale, SpriteEffects.None, 1.0f);
            spriteBatch.End();
        }

        private void HandleInput()
        {
            var keyboard = Keyboard.GetState();
            if (keyboard.IsKeyUp(Keys.Space) && this.previousKeyboard.IsKeyDown(Keys.Space))
            {
                this.SwitchToNextScreen();
            }

            this.previousKeyboard = keyboard;
        }

        private void HandleTimer(GameTime gameTime)
        {
            this.timer += gameTime.ElapsedGameTime.TotalSeconds;
            if (this.timer > 3.0d)
            {
                this.SwitchToNextScreen();
            }
        }

        public override void Initialize()
        {
            base.Initialize();
            this.texture = this.Game.Content.Load<Texture2D>(this.textureName);
            this.origin = this.texture.Bounds.Size.ToVector2() * 0.5f;
            this.spriteBatch = new SpriteBatch(this.GraphicsDevice);
        }

        private void SwitchToNextScreen()
        {
            if (this.isSwitching)
            {
                return;
            }
            this.isSwitching = true;

            var screenService = this.Game.Services.GetService<IScreenService>();
            screenService.SwitchTo(this.nextScreen);
        }

        protected override void UnloadContent()
        {
            this.spriteBatch.Dispose();
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            this.HandleTimer(gameTime);
            this.HandleInput();
        }
    }
}
