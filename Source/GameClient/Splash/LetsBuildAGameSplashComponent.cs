﻿using LetsBuildAGame.Screenery;
using Microsoft.Xna.Framework;

namespace LetsBuildAGame.Splash
{
    public class LetsBuildAGameSplashComponent : BaseSplashComponent
    {
        public LetsBuildAGameSplashComponent(Game game) : base(game, "Splash/LetsBuildAGame", Screens.MainMenu)
        {
        }
    }
}
