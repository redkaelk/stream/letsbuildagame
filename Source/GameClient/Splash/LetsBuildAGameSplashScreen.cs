﻿using LetsBuildAGame.Screenery;
using Microsoft.Xna.Framework;
using System;

namespace LetsBuildAGame.Splash
{
    public class LetsBuildAGameSplashScreen : Screen
    {
        public LetsBuildAGameSplashScreen()
        {
            this.BackgroundColor = Color.Black;
            this.ComponentTypes = new Type[]
            {
                typeof(LetsBuildAGameSplashComponent),
            };
        }
    }
}
