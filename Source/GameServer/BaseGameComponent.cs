﻿using Microsoft.Xna.Framework;
using System;

namespace LetsBuildAGame
{
    public abstract class BaseGameComponent : IGameComponent, IUpdateable
    {
        public bool Enabled
        {
            get;
        } = true;

        public Game Game
        {
            get;
        }

        public int UpdateOrder
        {
            get;
        } = 0;

        public BaseGameComponent(Game game)
        {
            if (game is null)
            {
                throw new ArgumentNullException(nameof(game));
            }

            this.Game = game;
        }

        public virtual void Initialize()
        {
        }

        public virtual void Update(GameTime gameTime)
        {
        }

#pragma warning disable CS0067 // not supported in our game server implementation
        public event EventHandler<EventArgs> EnabledChanged;
        public event EventHandler<EventArgs> UpdateOrderChanged;
#pragma warning restore CS0067
    }
}
