﻿namespace LetsBuildAGame
{
    public class IdService<T>
    {
        private long nextId;

        public long Create()
        {
            var id = ++this.nextId;
            return id;
        }
    }
}
