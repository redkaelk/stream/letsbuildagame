﻿using LetsBuildAGame.Networking;
using LetsBuildAGame.Networking.Messages;
using Lidgren.Network;
using System.Collections.Generic;

namespace LetsBuildAGame.MessageHandlers
{
    public class PlayerInputMessageHandler : BaseMessageHandler<PlayerInputMessage, Game>
    {
        private PlayerSessionCollections playerSessions;

        public PlayerInputMessageHandler(Game game) : base(game)
        {
        }

        protected override void Handle(PlayerInputMessage message, NetConnection senderConnection)
        {
            var playerSession = this.playerSessions.ByConnection.GetValueOrDefault(senderConnection);
            if (playerSession == null)
            {
                senderConnection.Disconnect("Player session not found");
                return;
            }

            var input = playerSession.Input;
            input.HorizontalDirection = message.HorizontalDirection;
            input.VerticalDirection = message.VerticalDirection;
        }

        public override void Initialize()
        {
            base.Initialize();

            var services = this.Game.Services;
            this.playerSessions = services.GetService<PlayerSessionCollections>();
        }
    }
}
