﻿using LetsBuildAGame.Networking;
using LetsBuildAGame.Networking.Messages;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using System.Diagnostics;

namespace LetsBuildAGame.MessageHandlers
{
    public class JoinRequestMessageHandler : BaseMessageHandler<JoinRequestMessage, Game>
    {
        private IdService<Sprite> spriteIdService;
        private SpriteCollections sprites;
        private PlayerSessionCollections playerSessions;
        private NetServer server;

        public JoinRequestMessageHandler(Game game) : base(game)
        {
        }

        protected override void Handle(JoinRequestMessage message, NetConnection senderConnection)
        {
            var displayName = message.DisplayName;
            Debug.WriteLine(displayName + " joined");

            var id = this.spriteIdService.Create();
            var sprite = new Sprite()
            {
                Id = id,
                Origin = new Vector2(0.5f, 0.5f),
                TextureName = "characterBlue (1)",
            };
            _ = this.sprites.All.Add(sprite);

            var playerSession = new PlayerSession()
            {
                Sprite = sprite,
            };
            _ = this.playerSessions.All.Add(playerSession);
            this.playerSessions.ByConnection.Add(senderConnection, playerSession);

            var joinResponse = new JoinResponseMessage()
            {
                MessageId = ServerToClientMessageId.JoinResponse,
                WasJoined = true,
                PlayerSpriteId = sprite.Id,
            };

            var reply = this.server.CreateMessage();
            reply.Write(joinResponse);
            _ = this.server.SendMessage(reply, senderConnection, NetDeliveryMethod.ReliableOrdered, 0);

            this.SendSprites(senderConnection);
        }

        private void SendSprites(NetConnection senderConnection)
        {
            foreach(var sprite in this.sprites.All)
            {
                var createSpriteMessage = new CreateSpriteMessage()
                {
                    MessageId = ServerToClientMessageId.CreateSprite,
                    LayerDepth = sprite.LayerDepth,
                    Scale = sprite.Scale,
                    Origin = sprite.Origin,
                    TextureName = sprite.TextureName,
                    Position = sprite.Position,
                    Rotation = sprite.Rotation,
                    Id = sprite.Id,
                };

                var message = this.server.CreateMessage();
                message.Write(createSpriteMessage);
                this.server.SendMessage(message, senderConnection, NetDeliveryMethod.ReliableUnordered, 1);
            }
        }

        public override void Initialize()
        {
            base.Initialize();

            var services = this.Game.Services;
            this.spriteIdService = services.GetService<IdService<Sprite>>();
            this.sprites = services.GetService<SpriteCollections>();
            this.playerSessions = services.GetService<PlayerSessionCollections>();
            this.server = services.GetService<NetServer>();
        }
    }
}
