﻿namespace LetsBuildAGame
{
    public class PlayerInput
    {
        public sbyte HorizontalDirection
        {
            get;
            set;
        }

        public sbyte VerticalDirection
        {
            get;
            set;
        }
    }
}
