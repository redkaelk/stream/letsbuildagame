﻿using Lidgren.Network;

namespace LetsBuildAGame
{
    public static class Program
    {
        public static void Main()
        {
            using (var game = new Game())
            {
                var networkConfiguration = new NetPeerConfiguration(NetworkConstants.AppIdentifier)
                {
                    Port = NetworkConstants.DefaultPort,
                };
                var networkServer = new NetServer(networkConfiguration);

                var services = game.Services;
                services.AddService(networkServer);
                services.AddService(new SpriteCollections());
                services.AddService(new IdService<Sprite>());
                services.AddService(new PlayerSessionCollections());

                var components = game.Components;
                components.Add(new ReceiverNetworkComponent(game));
                components.Add(new SpriteNetworkComponent(game));
                components.Add(new HumanPlayerComponent(game));
                components.Add(new VillageIdiotAIPlayerComponent(game));
                components.Add(new SkinnyForestComponent(game));
                components.Add(new ConsoleComponent(game));

                game.Run();
            }
        }
    }
}
