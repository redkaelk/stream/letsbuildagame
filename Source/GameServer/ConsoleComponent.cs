﻿using Microsoft.Xna.Framework;
using System;

namespace LetsBuildAGame
{
    public class ConsoleComponent : BaseGameComponent
    {
        private double timer;

        public ConsoleComponent(Game game) : base(game)
        {
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            this.timer += gameTime.ElapsedGameTime.TotalSeconds;
            if (this.timer >= 1.0d)
            {
                this.timer = 0.0d;
                Console.SetCursorPosition(0, 0);
                Console.Write(gameTime.TotalGameTime);
            }
        }
    }
}
