using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace LetsBuildAGame
{
    public class SkinnyForestComponent : BaseGameComponent
    {
        public SkinnyForestComponent(Game game) : base(game)
        {
        }

        public override void Initialize()
        {
            base.Initialize();

            var services = this.Game.Services;
            var spriteIdService = services.GetService<IdService<Sprite>>();
            var sprites = services.GetService<SpriteCollections>();
            var random = new Random();

            for(var index = 0; index < 10; index++)
            {
                var id = spriteIdService.Create();
                var x = random.NextSingle() * 500.0f;
                var y = random.NextSingle() * 500.0f;
                var rotation = random.NextSingle() * MathHelper.TwoPi;
                var scale = Vector2.One * ((random.NextSingle() * 0.4f) + 1.0f);
                var sprite = new Sprite()
                {
                    Color = Color.White,
                    Id = id,
                    LayerDepth = 1.0f,
                    Origin = new Vector2(0.5f, 0.5f),
                    Position = new Vector2(x, y),
                    Rotation = rotation,
                    Scale = scale,
                    SpriteEffects = SpriteEffects.None,
                    TextureName = "tree_small",
                };

                sprites.All.Add(sprite);
            }
        }
    }
}
