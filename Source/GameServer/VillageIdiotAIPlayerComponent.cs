﻿using Microsoft.Xna.Framework;
using System;

namespace LetsBuildAGame
{
    public class VillageIdiotAIPlayerComponent : BaseGameComponent
    {
        private Sprite sprite;
        private readonly Random random = new Random();
        private Vector2 targetPosition;

        public VillageIdiotAIPlayerComponent(Game game) : base(game)
        {
        }

        public override void Initialize()
        {
            base.Initialize();

            var services = this.Game.Services;
            var spriteIdService = services.GetService<IdService<Sprite>>();
            var sprites = services.GetService<SpriteCollections>();

            var id = spriteIdService.Create();
            this.sprite = new Sprite()
            {
                Id = id,
                Origin = new Vector2(0.5f, 0.5f),
                TextureName = "characterBlue (1)",
            };

            _ = sprites.All.Add(this.sprite);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            var distance = Vector2.Distance(this.sprite.Position, this.targetPosition);
            if (distance < 1.0f)
            {
                var width = 500;
                var height = 500;
                var x = this.random.Next(0, width);
                var y = this.random.Next(0, height);
                this.targetPosition = new Vector2(x, y);
                distance = Vector2.Distance(this.sprite.Position, this.targetPosition);
            }

            var direction = this.targetPosition - this.sprite.Position;
            if (direction != Vector2.Zero)
            {
                direction.Normalize();
            }
            var amount = 100.1192f * (float)gameTime.ElapsedGameTime.TotalSeconds;
            amount = MathF.Min(amount, distance);
            var velocity = direction * amount;
            this.sprite.Position += velocity;
            this.sprite.Rotation = MathF.Atan2(direction.Y, direction.X);
            this.sprite.Flags |= SpriteFlags.AllPositionChanged | SpriteFlags.AllRotationChanged;
        }
    }
}
