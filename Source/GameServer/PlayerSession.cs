﻿namespace LetsBuildAGame
{
    public class PlayerSession
    {
        public PlayerInput Input
        {
            get;
        } = new PlayerInput();

        public Sprite Sprite
        {
            get;
            set;
        }
    }
}
