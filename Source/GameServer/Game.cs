﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace LetsBuildAGame
{
    public class Game : IDisposable
    {
        private static readonly long frequency = Stopwatch.Frequency / 10_000_000;
        private readonly GameTime gameTime = new GameTime();
        private bool isDisposed;
        private bool isExiting;
        private readonly Stopwatch stopwatch = new Stopwatch();
        private readonly List<IUpdateable> updateables = new List<IUpdateable>();

        public GameComponentCollection Components
        {
            get;
        } = new GameComponentCollection();

        public GameServiceContainer Services
        {
            get;
        } = new GameServiceContainer();

        public Game()
        {
            this.Components.ComponentAdded += this.HandleComponentAdded;
            this.Components.ComponentRemoved += this.HandleComponentRemoved;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.isDisposed)
            {
                return;
            }

            if (disposing)
            {
                foreach (var component in this.Components)
                {
                    if (component is IDisposable disposable)
                    {
                        disposable.Dispose();
                    }
                }
            }

            this.isDisposed = true;
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            this.Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        public void Exit()
        {
            this.isExiting = true;
        }

        private void GameLoop()
        {
            while (!this.isExiting)
            {
                foreach (var component in this.updateables)
                {
                    component.Update(this.gameTime);
                }
                this.UpdateGameTime();

                var sleep = Math.Max(1, (1000/30) - (int)gameTime.ElapsedGameTime.TotalMilliseconds);
                Thread.Sleep(sleep);
            }
        }

        private void HandleComponentAdded(object sender, GameComponentCollectionEventArgs args)
        {
            var component = args.GameComponent;
            if (component is IUpdateable updateable)
            {
                this.updateables.Add(updateable);
            }

            if (this.stopwatch.IsRunning)
            {
                component.Initialize();
            }
        }

        private void HandleComponentRemoved(object sender, GameComponentCollectionEventArgs args)
        {
            var component = args.GameComponent;
            if (component is IUpdateable updateable)
            {
                _ = this.updateables.Remove(updateable);
            }
        }

        private void InitializeExistingComponents()
        {
            foreach (var component in this.Components)
            {
                component.Initialize();
            }
        }

        public void Run()
        {
            this.InitializeExistingComponents();
            this.stopwatch.Start();
            this.GameLoop();
        }

        private void UpdateGameTime()
        {
            var total = this.stopwatch.ElapsedTicks / frequency;
            var elapsed = total - this.gameTime.TotalGameTime.Ticks;
            this.gameTime.ElapsedGameTime = new TimeSpan(elapsed);
            this.gameTime.TotalGameTime += this.gameTime.ElapsedGameTime;
        }
    }
}
