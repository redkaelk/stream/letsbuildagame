﻿using LetsBuildAGame.Networking;
using LetsBuildAGame.Networking.Messages;
using Lidgren.Network;
using Microsoft.Xna.Framework;

namespace LetsBuildAGame
{
    public class SpriteNetworkComponent : BaseGameComponent
    {
        private SpriteCollections sprites;
        private NetServer server;

        public SpriteNetworkComponent(Game game) : base(game)
        {
        }

        public override void Initialize()
        {
            base.Initialize();

            var services = this.Game.Services;
            this.sprites = services.GetService<SpriteCollections>();
            this.server = services.GetService<NetServer>();
        }

        private void SendSprite(Sprite sprite)
        {
            var skip
                = (!sprite.Flags.HasFlag(SpriteFlags.NetworkPositionChanged))
                && (!sprite.Flags.HasFlag(SpriteFlags.NetworkRotationChanged))
                ;
            if(skip)
            {
                return;
            }
            sprite.Flags &= ~SpriteFlags.NetworkPositionChanged;
            sprite.Flags &= ~SpriteFlags.NetworkRotationChanged;

            var updateSpritePositionRotation = new UpdateSpritePositionRotationMessage()
            {
                MessageId = ServerToClientMessageId.UpdateSpritePositionRotation,
                Position = sprite.Position,
                Rotation = sprite.Rotation,
                Id = sprite.Id,
            };

            var message = this.server.CreateMessage();
            message.Write(updateSpritePositionRotation);
            this.server.SendToAll(message, null, NetDeliveryMethod.UnreliableSequenced, 1);
        }

        private void SendSprites()
        {
            foreach (var sprite in this.sprites.All)
            {
                this.SendSprite(sprite);
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            this.SendSprites();
        }
    }
}
