﻿using System.Collections.Generic;

namespace LetsBuildAGame
{
    public class SpriteCollections
    {
        public HashSet<Sprite> All
        {
            get;
        } = new HashSet<Sprite>();
    }
}
