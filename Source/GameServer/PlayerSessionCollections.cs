﻿using Lidgren.Network;
using System.Collections.Generic;

namespace LetsBuildAGame
{
    public class PlayerSessionCollections
    {
        public HashSet<PlayerSession> All
        {
            get;
        } = new HashSet<PlayerSession>();

        public Dictionary<NetConnection, PlayerSession> ByConnection
        {
            get;
        } = new Dictionary<NetConnection, PlayerSession>();
    }
}
