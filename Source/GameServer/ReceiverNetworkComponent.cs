﻿using LetsBuildAGame.MessageHandlers;
using LetsBuildAGame.Networking;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace LetsBuildAGame
{
    public class ReceiverNetworkComponent : BaseGameComponent
    {
        private readonly IMessageHandler[] handlers = new IMessageHandler[256];
        private NetServer server;

        public ReceiverNetworkComponent(Game game) : base(game)
        {
            this.SetHandler<JoinRequestMessageHandler>(ClientToServerMessageId.JoinRequest);
            this.SetHandler<PlayerInputMessageHandler>(ClientToServerMessageId.PlayerInput);
        }

        private void HandleData(NetIncomingMessage message)
        {
            var index = message.PeekByte();
            var handler = this.handlers[index];
            if (handler == null)
            {
                return;
            }

            handler.Handle(message);
        }

        public override void Initialize()
        {
            base.Initialize();
            this.InitializeExistingHandlers();

            var services = this.Game.Services;
            this.server = services.GetService<NetServer>();
            this.server.Start();
        }

        private void InitializeExistingHandlers()
        {
            foreach (var handler in this.handlers)
            {
                if (handler == null)
                {
                    continue;
                }

                handler.Initialize();
            }
        }

        private void SetHandler<THandler>(ClientToServerMessageId id) where THandler : IMessageHandler
        {
            var index = (byte)id;
            var handler = (IMessageHandler)Activator.CreateInstance(typeof(THandler), this.Game);
            this.handlers[index] = handler;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            var messages = new List<NetIncomingMessage>();
            _ = this.server.ReadMessages(messages);
            foreach (var message in messages)
            {
                switch (message.MessageType)
                {
                    case NetIncomingMessageType.ConnectionApproval:
                        message.SenderConnection.Approve();
                        break;
                    case NetIncomingMessageType.Data:
                        this.HandleData(message);
                        break;
                }

                this.server.Recycle(message);
            }
        }
    }
}
