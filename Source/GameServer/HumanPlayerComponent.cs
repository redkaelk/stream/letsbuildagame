﻿using Microsoft.Xna.Framework;
using System;

namespace LetsBuildAGame
{
    public class HumanPlayerComponent : BaseGameComponent
    {
        private PlayerSessionCollections playerSessions;

        public HumanPlayerComponent(Game game) : base(game)
        {
        }

        public override void Initialize()
        {
            base.Initialize();

            var services = this.Game.Services;
            this.playerSessions = services.GetService<PlayerSessionCollections>();
        }

        private void UpdatePlayerSession(PlayerSession playerSession, GameTime gameTime)
        {
            var input = playerSession.Input;
            var direction = new Vector2(input.HorizontalDirection, input.VerticalDirection);
            if (direction != Vector2.Zero)
            {
                direction.Normalize();
                var amount = 100.1192f * (float)gameTime.ElapsedGameTime.TotalSeconds;
                var velocity = direction * amount;

                var sprite = playerSession.Sprite;
                sprite.Position += velocity;
                sprite.Rotation = MathF.Atan2(direction.Y, direction.X);
                sprite.Flags |= SpriteFlags.AllPositionChanged | SpriteFlags.AllRotationChanged;
            }
        }

        private void UpdatePlayerSessions(GameTime gameTime)
        {
            foreach (var playerSession in this.playerSessions.All)
            {
                this.UpdatePlayerSession(playerSession, gameTime);
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            this.UpdatePlayerSessions(gameTime);
        }
    }
}
